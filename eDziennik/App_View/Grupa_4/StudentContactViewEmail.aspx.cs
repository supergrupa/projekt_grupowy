﻿using eDziennik.App_Code.Grupa_4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using eDziennik.App_SchoolDb;
using System.Net.Mail;
using eDziennik.App_View.Grupa_4.Helpers;
using System.Web.ModelBinding;

namespace eDziennik.App_View.Grupa_4
{
    public partial class StudentContactViewEmail : System.Web.UI.Page, IHelper
    {
        private String fromSb;
        private String toSb;
        private String title;
        private String text;
        private ContentPlaceHolder cph;

        private Teacher teacher = null;
        private readonly IStudentContact _studentContact;

        public Info Info { get; set; }

        public StudentContactViewEmail()
        {
            this._studentContact = new StudentContact();
        }

        public Teacher GetTeacher([QueryString("ID")] string teacherId)
        {
            Teacher teacher = null;
            int id = 0;
            int.TryParse(teacherId, out id);
            if (id > 0)
                teacher = _studentContact.GetTeacherById(id);

            if (teacher == null || id == 0)
            {
                Info = new Info
                {
                    InfoTitle = "Błędny numer ID nauczyciela",
                    InfoDescription = "Możliwe, że próbujesz pobrać informacje o nauczycielu, który nie istnieje w bazie."
                };

                Server.Transfer("SchoolInfo.aspx");
            }

            return teacher;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            cph = (ContentPlaceHolder)Master.FindControl("ContentPlaceHolder1");
            teacher = GetTeacher(Request.QueryString["ID"]);
            if (teacher != null)
                ((Label)cph.FindControl("Lto")).Text = teacher.TeacherFirstName + " " + teacher.TeacherLastName;

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            cph = (ContentPlaceHolder)Master.FindControl("ContentPlaceHolder1");
            title = ((TextBox)cph.FindControl("TextBox1")).Text;
            text = ((TextBox)cph.FindControl("TextBox2")).Text;
            toSb = _studentContact.GetTeacherById(teacher.TeacherId).TeacherEmail;

            if (toSb == null)
            {
                Info = new Info
                {
                    InfoTitle = "Email nie może zostać wysłany",
                    InfoDescription = "Adres email nauczyciela nie jest znany."
                };
                Server.Transfer("SchoolInfo.aspx");
            }

            if (title.Length == 0 || text.Length == 0)
            {
                Info = new Info
                {
                    InfoTitle = "Tytuł i tekst nie mogą zostać puste",
                    InfoDescription = "Wypełnij pola tytuł i tekst."
                };
                Server.Transfer("SchoolInfo.aspx");
            }

            if (Session["Id"] != null && Session["RoleName"]=="student")
            {
                int actualId = Convert.ToInt32(Session["Id"]);
                fromSb = _studentContact.GetStudentById(actualId).StudentEmail;

                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
                MailMessage mail = new MailMessage();
                mail.To.Add(new MailAddress(toSb));
                mail.Subject = title + " From: " + fromSb;
                mail.Body = text;
                smtpClient.Send(mail);

                Response.Redirect("/App_View/Grupa_4/StudentContactView.aspx");
            }
            else
            {
                Info = new Info
                {
                    InfoTitle = "Nie jestes zalogowany",
                    InfoDescription = "Zaloguj sie."
                };
                Server.Transfer("SchoolInfo.aspx");
            }

        }
    }
}