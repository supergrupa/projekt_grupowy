﻿using eDziennik.App_Code.Grupa_4;
using eDziennik.App_SchoolDb;
using eDziennik.App_View.Grupa_4.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace eDziennik.App_View.Grupa_4
{
    public partial class StudentContactView : System.Web.UI.Page, IHelper
    {
        private readonly IStudentContact _studentContact;
        private ContentPlaceHolder cph;
        public Info Info { get; set; }
        public StudentContactView()
        {
            this._studentContact = new StudentContact();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            cph = (ContentPlaceHolder)Master.FindControl("ContentPlaceHolder1");
            DropDownList ddl = ((DropDownList)cph.FindControl("DropDownList1"));

            Teacher[] teachers = _studentContact.GetAllTeachers();

            foreach (var t in teachers)
            {
                ddl.Items.Add(new ListItem(t.TeacherFirstName + " " + t.TeacherLastName, t.TeacherId.ToString()));
            }


        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ContentPlaceHolder cph = (ContentPlaceHolder)Master.FindControl("ContentPlaceHolder1");
            ((Panel)cph.FindControl("Panel1")).Visible = true;
            DropDownList ddl = ((DropDownList)cph.FindControl("DropDownList1"));

            int teacherId = Convert.ToInt32(ddl.SelectedValue);
            Teacher teacher = _studentContact.GetTeacherById(teacherId);

            ((Label)cph.FindControl("Lname")).Text = teacher.TeacherFirstName;
            ((Label)cph.FindControl("Lsurname")).Text = teacher.TeacherLastName;
            ((Label)cph.FindControl("Lphone")).Text = teacher.TeacherPhoneNumber;
            ((Label)cph.FindControl("Lemail")).Text = teacher.TeacherEmail;
            ((Label)cph.FindControl("Laddress")).Text = String.Format("{0} {1}/{2}", teacher.TeacherAddress_Street, teacher.TeacherAddress_StreetNumber, teacher.TeacherAddress_FlatNumber);
            ((Label)cph.FindControl("Lcountry")).Text = String.Format("{0}, {1}, {2}", teacher.TeacherAddress_City, teacher.TeacherAddress_State, teacher.TeacherAddress_Country);

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            int teacherId;
            cph = (ContentPlaceHolder)Master.FindControl("ContentPlaceHolder1");
            DropDownList ddl = ((DropDownList)cph.FindControl("DropDownList1"));

            if (ddl.SelectedIndex != -1)
            {
                teacherId = Convert.ToInt32(ddl.SelectedValue);
                string url = "/App_View/Grupa_4/StudentContactViewEmail.aspx?";
                url += "ID=" + teacherId;
                Response.Redirect(url);
            }
            else
            {
                Info = new Info
                {
                    InfoTitle = "Nauczyciel nie został wybrany",
                    InfoDescription = "Wybierz nauczyciela z listy, do którego chcesz wysłać email."
                };

            }


        }
    }
}