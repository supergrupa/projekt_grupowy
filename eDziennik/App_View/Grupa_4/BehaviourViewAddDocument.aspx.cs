﻿using eDziennik.App_Code.Grupa_4;
using eDziennik.App_SchoolDb;
using eDziennik.App_View.Grupa_4.Helpers;
using System;
using System.Web.ModelBinding;
using System.Web.UI.WebControls;

namespace eDziennik.App_View.Grupa_4
{
    public partial class BehaviourViewAddDocument : System.Web.UI.Page, IHelper
    {
        private readonly IBehaviour _behaviour;
        private int studentId;
        private int dType;
        private int sessionId;
        private DocumentType documentType;
        private ContentPlaceHolder cph;
        public Info Info { get; set; }

        public BehaviourViewAddDocument()
        {
            this._behaviour = new Behaviour();
        }

        public Student GetStudent([QueryString("ID")] string studentId)
        {
            Student student = null;
            int id = 0;
            int.TryParse(studentId, out id);

            if (id > 0)
                student = _behaviour.GetStudent(id);

            if (student == null || id == 0)
            {
                Info = new Info
                {
                    InfoTitle = "Błędny numer ID ucznia",
                    InfoDescription = "Możliwe, że próbujesz pobrać informacje o uczniu, który nie istnieje w bazie."
                };

                Server.Transfer("SchoolInfo.aspx");
            }

            return student;
        }

        private DocumentType GetD([QueryString("d")] string d)
        {
            bool result = int.TryParse(d, out dType);

            if (result)
            {
                dType = Convert.ToInt32(d);
                if (dType == (int)DocumentType.Commendation)
                {
                    documentType = DocumentType.Commendation;
                }
                else if (dType == (int)DocumentType.Reprimand)
                {
                    documentType = DocumentType.Reprimand;
                }
                else
                {
                    Info = new Info
                    {
                        InfoTitle = "Błędny adres url",
                        InfoDescription = "Zła wartość parametru d."
                    };

                    Server.Transfer("SchoolInfo.aspx");
                }
            }
            else
            {
                Info = new Info
                {
                    InfoTitle = "Błędny adres url",
                    InfoDescription = "Zła wartość parametru d."
                };

                Server.Transfer("SchoolInfo.aspx");
            }
            return documentType;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Student student = null;
            student = GetStudent(Request.QueryString["ID"]);
            studentId = student.StudentId;

            documentType = GetD(Request.QueryString["d"]);

            //if (Session["ID"] != null && Session["RoleName"] != null)
            //{
            //    if (Session["RoleName"].ToString() == "teacher")
            //    {
            //        tutaj dalsza czesc Page_Load
            //    }
            //    else
            //    {
            //        Info = new Info
            //        {
            //            InfoTitle = "Nie jesteś zalogowany jako nauczyciel",
            //            InfoDescription = "Aby dodawać pochwały i nagany uczniom, musisz być zalogowany jako nauczyciel."
            //        };

            //        Server.Transfer("SchoolInfo.aspx");
            //    }
            //}

            sessionId = 1;

            var teacher = _behaviour.GetTeacher(sessionId);

            cph = (ContentPlaceHolder)Master.FindControl("ContentPlaceHolder1");

            Label teacherName = ((Label)cph.FindControl("Label2"));
            teacherName.Text = "Wystawiane przez: " + teacher.TeacherFirstName + " " + teacher.TeacherLastName;

            Label studentName = ((Label)cph.FindControl("Label3"));

            if ((int)documentType == 1)
            {
                studentName.Text = "Nagana dla ucznia: " + student.StudentFirstName + " " + student.StudentLastName;
            }
            else
            {
                studentName.Text = "Pochwała dla ucznia: " + student.StudentFirstName + " " + student.StudentLastName;
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var document = _behaviour.GetEmptyDocument();

            cph = (ContentPlaceHolder)Master.FindControl("ContentPlaceHolder1");
            Calendar calendar = ((Calendar)cph.FindControl("Calendar1"));
            string documentContent = ((TextBox)cph.FindControl("TextBox1")).Text;

            if (documentContent == "")
            {
                Info = new Info
                {
                    InfoTitle = "Treść wpisu nie została podana",
                    InfoDescription = "Pusty wpis nie może zostać dodany."
                };

                Server.Transfer("SchoolInfo.aspx");
            }

            if (calendar.SelectedDate.Date == DateTime.MinValue)
            {
                calendar.SelectedDate = DateTime.Today;
            }


            document.StudentId = studentId;
            document.TeacherId = sessionId;
            document.DocumentType = documentType;
            document.DocumentDate = calendar.SelectedDate.Date;
            document.DocumentContent = documentContent;

            var result = _behaviour.AddDocument(document);

            if (result)
            {
                Info = new Info
                {
                    InfoTitle = "Wpis został dodany!",
                };
            }
            else
            {
                Info = new Info
                {
                    InfoTitle = "Wpis nie został dodany!",
                    InfoDescription = "Prawdopodobnie podałeś złe dane dotyczące dokumentu."
                };
            }


            Server.Transfer("SchoolInfo.aspx");
        }
    }
}