﻿using eDziennik.App_Code.Grupa_4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace eDziennik.App_View.Grupa_4
{
    public partial class BehaviourViewTeacher : System.Web.UI.Page
    {
        private int teacherId;
        private readonly IBehaviour _behaviour;
        private ContentPlaceHolder cph;

        public BehaviourViewTeacher()
        {
            this._behaviour = new Behaviour();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                cph = (ContentPlaceHolder)Master.FindControl("ContentPlaceHolder1");
                DropDownList ddl = ((DropDownList)cph.FindControl("DdlClasses"));
                DropDownList ddlStudents = ((DropDownList)cph.FindControl("DdlStudents"));

                var classes = _behaviour.GetAllClasses();

                ddl.Items.Add(new ListItem("Wybierz klasę", "0"));
                foreach (var c in classes)
                {
                    ddl.Items.Add(new ListItem("Klasa: " + c.ClassSymbol + "-" + c.ClassSpecialization, c.ClassId.ToString()));
                }
            }
        }

        protected void DdlClasses_SelectedIndexChanged(object sender, EventArgs e)
        {
            cph = (ContentPlaceHolder)Master.FindControl("ContentPlaceHolder1");
            DropDownList ddlClasses = ((DropDownList)cph.FindControl("DdlClasses"));
            DropDownList ddlStudents = ((DropDownList)cph.FindControl("DdlStudents"));

            int classId = Convert.ToInt32(ddlClasses.SelectedValue);

            ddlStudents.Items.Clear();
            
            if(classId != 0)
            {
                var students = _behaviour.GetAllStudentsFromClass(classId);

                if (students.Count() != 0)
                {
                    foreach (var s in students)
                    {
                        ddlStudents.Items.Add(new ListItem(s.StudentFirstName + " " + s.StudentLastName, s.StudentId.ToString()));
                    }
                }
                else
                {
                    ddlStudents.Items.Add(new ListItem("Brak uczniów w wybranej klasie", "0"));
                }
                
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            cph = (ContentPlaceHolder)Master.FindControl("ContentPlaceHolder1");
            DropDownList ddlStudents = ((DropDownList)cph.FindControl("DdlStudents"));
            int studentId;

            if (ddlStudents.Items.Count != 0)
            {
                studentId = Convert.ToInt32(ddlStudents.SelectedValue);

                if (studentId != 0)
                {
                    string url = "/App_View/Grupa_4/BehaviourViewDocument.aspx?";
                    url += "ID=" + studentId;
                    Response.Redirect(url);
                }
                
            }

            
        }
    }
}