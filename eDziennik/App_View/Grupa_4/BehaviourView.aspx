﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SchoolMasterPage.Master" AutoEventWireup="true" CodeBehind="BehaviourView.aspx.cs" Inherits="eDziennik.App_View.Grupa_4.BehaviourView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="DocumentId" ForeColor="Black" GridLines="Vertical">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="DocumentId" HeaderText="DocumentId" ReadOnly="True" SortExpression="DocumentId" Visible="False" />
            <asp:BoundField DataField="StudentId" HeaderText="StudentId" SortExpression="StudentId" Visible="False" />
            <asp:BoundField DataField="TeacherId" HeaderText="TeacherId" SortExpression="TeacherId" Visible="False" />
            <asp:BoundField DataField="DocumentType" HeaderText="DocumentType" SortExpression="DocumentType" />
            <asp:BoundField DataField="DocumentDate" HeaderText="DocumentDate" SortExpression="DocumentDate" />
            <asp:BoundField DataField="DocumentContent" HeaderText="DocumentContent" SortExpression="DocumentContent" />
        </Columns>
        <FooterStyle BackColor="#CCCC99" />
        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
        <RowStyle BackColor="#F7F7DE" />
        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#FBFBF2" />
        <SortedAscendingHeaderStyle BackColor="#848384" />
        <SortedDescendingCellStyle BackColor="#EAEAD3" />
        <SortedDescendingHeaderStyle BackColor="#575357" />
    </asp:GridView>
    <asp:EntityDataSource ID="EntityDataSource1" runat="server" ConnectionString="name=SchoolDb" DefaultContainerName="SchoolDb" EnableFlattening="False" EntitySetName="Documents">
    </asp:EntityDataSource>
</asp:Content>
