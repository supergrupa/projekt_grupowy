﻿using eDziennik.App_Code.Grupa_4;
using eDziennik.App_SchoolDb;
using eDziennik.App_View.Grupa_4.Helpers;
using System;
using System.Web.ModelBinding;
using System.Web.UI.WebControls;


namespace eDziennik.App_View.Grupa_4
{
    public partial class AdminManageSchoolView : System.Web.UI.Page, IHelper
    {
        public Info Info { get; set; }

        private readonly IAdminManageSchool _adminManageSchool;

        public AdminManageSchoolView()
        {
            _adminManageSchool = new AdminManageSchool();
        }

        public School GetSchool([QueryString("schoolId")] int? schoolId)
        {
            School school = null;

            if (schoolId.HasValue && schoolId.Value > 0)
                school = _adminManageSchool.GetSchool(schoolId.Value);

            // jeśli nie ma szkoły o podanym ID
            if (school == null)
            {
                Info = new Info
                {
                    InfoTitle = "Błędny numer ID szkoły",
                    InfoDescription = "Możliwe, że próbujesz pobrać informacje o szkole, która nie istnieje.\nAby wyświetlić stronę należy podać ID szkoły. [schoolId] powinno zostać podane w QueryString."
                };

                Server.Transfer("SchoolInfo.aspx");
            }

            return school;
        }


        protected void Button2_Click(object sender, EventArgs e)
        {
            var schoolIDField = (HiddenField)schoolDetails.FindControl("SchoolID");

            var city = ((TextBox)schoolDetails.FindControl("TB_City")).Text;
            var country = ((TextBox)schoolDetails.FindControl("TB_Country")).Text;
            var dDirector = ((TextBox)schoolDetails.FindControl("TB_DDirector")).Text;
            var director = ((TextBox)schoolDetails.FindControl("TB_Director")).Text;
            var fNumber = ((TextBox)schoolDetails.FindControl("TB_FNumber")).Text;
            var name = ((TextBox)schoolDetails.FindControl("TB_Name")).Text;
            var pNumber = ((TextBox)schoolDetails.FindControl("TB_PNumber")).Text;
            var sNumber = ((TextBox)schoolDetails.FindControl("TB_SNumber")).Text;
            var state = ((TextBox)schoolDetails.FindControl("TB_State")).Text;
            var street = ((TextBox)schoolDetails.FindControl("TB_Street")).Text;

            School school = new School
            {
                SchoolName = name,
                SchoolDirector = director,
                SchoolDeputyDirector = dDirector,
                SchoolAddress_Street = street,
                SchoolAddress_StreetNumber = sNumber,
                SchoolAddress_FlatNumber = fNumber,
                SchoolAddress_PostNumber = pNumber,
                SchoolAddress_City = city,
                SchoolAddress_State = state,
                SchoolAddress_Country = country
            };

            if (schoolIDField.Value != null)
            {
                // kiedy wiemy już że Id istnieje to możemy je zparsować bezpiecznie
                school.SchoolId = int.Parse(schoolIDField.Value);

                var result = _adminManageSchool.UpdateSchool(school);

                // jeśli update się nie powiódł
                if (!result)
                {
                    Info = new Info
                    {
                        InfoTitle = "Nie zaktualizowano informacji!",
                        InfoDescription = "Podane dane były nieprawidłowe. Spróbuj zaaktualizować szkołę jeszcze raz.",
                        AdditionalInformation = "~/App_View/Grupa_4/AdminManageSchoolView.aspx?schoolId=" + schoolIDField.Value
                    };

                    Server.Transfer("SchoolInfo.aspx");
                }

                // ...a jeśli się powiódł to przekazujey do strony z infomacją
                Info = new Info
                {
                    InfoTitle = "Zaktualizowano informacje!",
                    InfoDescription = "Aby powrócić do poprzedniej strony kliknij poniższy link.",
                    AdditionalInformation = "~/App_View/Grupa_4/AdminManageSchoolView.aspx?schoolId=" + schoolIDField.Value
                };

                Server.Transfer("SchoolInfo.aspx");
            }
            // jeśli chcemy update'ować bez SchoolId
            else
            {
                Info = new Info
                {
                    InfoTitle = "Podane dane są nieprawidłowe",
                    InfoDescription = "Podane dane są nieprawidłowe. Możliwe, że próbujesz zaaktualizować informacje o szkole która nie istnieje."
                };

                Server.Transfer("SchoolInfo.aspx");
            }
        }
    }
}