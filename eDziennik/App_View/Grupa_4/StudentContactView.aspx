﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SchoolMasterPage.Master" AutoEventWireup="true" CodeBehind="StudentContactView.aspx.cs" Inherits="eDziennik.App_View.Grupa_4.StudentContactView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Wybierz nauczyciela z listy</h2>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList>
        <asp:Button ID="Button1" runat="server" Text="Sprawdź" OnClick="Button1_Click" />
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel runat="server" ID="Panel1" Visible="false" CssClass="partner"> 
            <div>
                <asp:Label ID="Label1" runat="server" Text="First Name"></asp:Label>
                <asp:Label ID="Lname" runat="server" Text="Label"></asp:Label>
                <br />
                <asp:Label ID="Label3" runat="server" Text="Last Name"></asp:Label>
                <asp:Label ID="Lsurname" runat="server" Text="Label"></asp:Label>
                <br />
                <asp:Label ID="Label5" runat="server" Text="Phone"></asp:Label>
                <asp:Label ID="Lphone" runat="server" Text="Label"></asp:Label>
                <br />
                <asp:Label ID="Label7" runat="server" Text="Email"></asp:Label>
                <asp:Label ID="Lemail" runat="server" Text="Label"></asp:Label>
                <br />
                <asp:Label ID="Label9" runat="server" Text="Address"></asp:Label>
                <asp:Label ID="Laddress" runat="server" Text="Label"></asp:Label>
                <br />
                <asp:Label ID="Lcountry" runat="server" Text="Label"></asp:Label>
                <br />
                <asp:Button ID="Button2" runat="server" Text="Wyślij Email" OnClick="Button2_Click" />
</asp:Panel>
            </div>
            
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Button1" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
