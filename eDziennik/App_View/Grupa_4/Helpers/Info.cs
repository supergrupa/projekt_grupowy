﻿
namespace eDziennik.App_View.Grupa_4.Helpers
{
    /// <summary>
    /// Typ przechowywujący informacje, błędy i wszystko co byśmy chcieli pokazać użytkownikowi
    /// </summary>
    public class Info
    {
        /// <summary>
        /// Tytuł informacji
        /// </summary>
        public string InfoTitle { get; set; }

        /// <summary>
        /// Opis błędu
        /// </summary>
        public string InfoDescription { get; set; }

        /// <summary>
        /// Pole na dodatkowe informacje dla użytkownika
        /// MB: ja używam do przekazania callback'a
        /// </summary>
        public string AdditionalInformation { get; set; }
    }
}