﻿<%@ Page
    Title="AdminManageSchool"
    Language="C#"
    MasterPageFile="~/SchoolMasterPage.Master"
    AutoEventWireup="true"
    CodeBehind="AdminManageSchoolView.aspx.cs"
    Inherits="eDziennik.App_View.Grupa_4.AdminManageSchoolView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:FormView ID="schoolDetails" runat="server" ItemType="eDziennik.App_SchoolDb.School" SelectMethod="GetSchool" RenderOuterTable="false">
        <ItemTemplate>
            <div>
                <h1>Edycja szkoły</h1>
            </div>
            <div>
                <asp:HiddenField ID="SchoolID" runat="server" Value="<%# Item.SchoolId %>" />

                <asp:Label ID="Label1" runat="server" Text="Name:"></asp:Label>
                <asp:TextBox ID="TB_Name" runat="server" Text="<%# Item.SchoolName %>"></asp:TextBox>
                <br />
                <asp:Label ID="Label2" runat="server" Text="Director:"></asp:Label>
                <asp:TextBox ID="TB_Director" runat="server" Text="<%# Item.SchoolDirector %>"></asp:TextBox>
                <br />
                <asp:Label ID="Label3" runat="server" Text="Deputy Director:"></asp:Label>
                <asp:TextBox ID="TB_DDirector" runat="server" Text="<%# Item.SchoolDeputyDirector %>"></asp:TextBox>
                <br />
                <asp:Label ID="Label4" runat="server" Text="Street:"></asp:Label>
                <asp:TextBox ID="TB_Street" runat="server" Text="<%# Item.SchoolAddress_Street %>"></asp:TextBox>
                <br />
                <asp:Label ID="Label5" runat="server" Text="Street Number:"></asp:Label>
                <asp:TextBox ID="TB_SNumber" runat="server" Text="<%# Item.SchoolAddress_StreetNumber %>"></asp:TextBox>
                <br />
                <asp:Label ID="Label6" runat="server" Text="Flat Number:"></asp:Label>
                <asp:TextBox ID="TB_FNumber" runat="server" Text="<%# Item.SchoolAddress_FlatNumber %>"></asp:TextBox>
                <br />
                <asp:Label ID="Label7" runat="server" Text="Post Number:"></asp:Label>
                <asp:TextBox ID="TB_PNumber" runat="server" Text="<%# Item.SchoolAddress_PostNumber %>"></asp:TextBox>
                <br />
                <asp:Label ID="Label8" runat="server" Text="City:"></asp:Label>
                <asp:TextBox ID="TB_City" runat="server" Text="<%# Item.SchoolAddress_City %>"></asp:TextBox>
                <br />
                <asp:Label ID="Label9" runat="server" Text="State:"></asp:Label>
                <asp:TextBox ID="TB_State" runat="server" Text="<%# Item.SchoolAddress_State %>"></asp:TextBox>
                <br />
                <asp:Label ID="Label10" runat="server" Text="Country:"></asp:Label>
                <asp:TextBox ID="TB_Country" runat="server" Text="<%# Item.SchoolAddress_Country %>"></asp:TextBox>
                <br />
                <asp:Button ID="Button2" runat="server" Text="Zapisz szkołę" OnClick="Button2_Click" />
            </div>
        </ItemTemplate>
    </asp:FormView>
</asp:Content>
