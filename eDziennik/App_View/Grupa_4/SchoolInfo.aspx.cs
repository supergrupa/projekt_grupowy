﻿using eDziennik.App_View.Grupa_4.Helpers;
using System;

namespace eDziennik.App_View.Grupa_4
{
    public partial class SchoolInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            IHelper amsv = (IHelper)Context.Handler;

            Label1.Text = amsv.Info.InfoTitle;
            Label2.Text = amsv.Info.InfoDescription;

            if (amsv.Info.AdditionalInformation != null)
                HyperLink1.NavigateUrl = amsv.Info.AdditionalInformation;
            else
                HyperLink1.Visible = false;
        }
    }
}