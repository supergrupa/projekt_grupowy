﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AuthenticationViewLogin.aspx.cs" Inherits="eDziennik.AuthenticationView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="~/App_Content/school.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="login_div">
            <asp:Login
                ID="Login1"
                runat="server"
                MembershipProvider="schoolMembership"
                BackColor="#F7F6F3"
                BorderColor="#E6E2D8"
                BorderPadding="4"
                BorderStyle="Solid"
                BorderWidth="1px"
                Font-Names="Verdana"
                Font-Size="0.8em"
                ForeColor="#333333"
                Height="152px"
                Width="288px">

                <InstructionTextStyle
                    Font-Italic="True"
                    ForeColor="Black" />

                <LoginButtonStyle
                    BackColor="#FFFBFF"
                    BorderColor="#CCCCCC"
                    BorderStyle="Solid"
                    BorderWidth="1px"
                    Font-Names="Verdana"
                    Font-Size="0.8em"
                    ForeColor="#284775" />

                <TextBoxStyle
                    Font-Size="0.8em" />

                <TitleTextStyle
                    BackColor="#5D7B9D"
                    Font-Bold="True"
                    Font-Size="0.9em"
                    ForeColor="White" />

            </asp:Login>
        </div>
    </form>
</body>
</html>
