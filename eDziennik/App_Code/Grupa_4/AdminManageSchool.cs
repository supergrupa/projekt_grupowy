﻿using eDziennik.App_SchoolDb;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace eDziennik.App_Code.Grupa_4
{
    public class AdminManageSchool : IAdminManageSchool
    {
        private readonly SchoolDb db;

        public AdminManageSchool()
        {
            db = new SchoolDb();
        }

        public void AddSchool(School school)
        {
            db.Schools.Add(school);
            db.SaveChanges();
        }

        public School GetSchool(int id)
        {
            return db.Schools.Where(s => s.SchoolId == id).SingleOrDefault();
        }

        public IQueryable<School> GetSchool(string schoolName)
        {
            return db.Schools.Where(s => s.SchoolName == schoolName);
        }

        public School GetEmptySchool()
        {
            return new School();
        }

        public bool UpdateSchool(School school)
        {
            try
            {
                db.Schools.Attach(school);

                db.Entry(school).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }

    [TestClass]
    public class AdminManageSchoolTests
    {
        [TestClass]
        public class IntegrationTests
        {
            private readonly IAdminManageSchool _adminManageSchool;
            private readonly SchoolDb _db;

            private readonly string SchoolName = "TestSchoolName";
            private int SchoolId;

            public IntegrationTests()
            {
                _adminManageSchool = new AdminManageSchool();
                _db = new SchoolDb();
            }

            [TestInitialize]
            public void BeforeEachTest()
            {
                var school = new School
                {
                    SchoolName = SchoolName
                };

                TestHelpers.RemoveSchool(SchoolName);
                TestHelpers.AddSchoolToDB(school);

                SchoolId = TestHelpers.GetSchoolId(SchoolName);
            }


            [TestMethod]
            public void AddSchool_WithValidObject_AddObjectToDB()
            {
                // Arrange
                var schoolsInDb = _db.Schools.Count();
                var school = new School
                {
                    SchoolName = "AddSchool_TestSchool"
                };

                // Act
                _adminManageSchool.AddSchool(school);

                Assert.AreEqual<int>(schoolsInDb + 1, _db.Schools.Count());

                // Clean db
                TestHelpers.RemoveSchool(school.SchoolName);
            }

            [TestMethod]
            public void GetSchool_WithValidId_ReturnsDesiredSchool()
            {
                var school = _adminManageSchool.GetSchool(SchoolId);

                Assert.AreEqual(SchoolId, school.SchoolId);
                Assert.AreEqual(SchoolName, school.SchoolName);
            }

            [TestMethod]
            public void GetSchool_WithValidName_ReturnsDesiredSchool()
            {
                var school = _adminManageSchool.GetSchool(SchoolName).First();


                Assert.AreEqual(SchoolId, school.SchoolId);
                Assert.AreEqual(SchoolName, school.SchoolName);
            }

            [TestMethod]
            public void GetSchool_WithNonExistingId_ReturnsNull()
            {
                var nonExistingId = Int32.MaxValue;

                var school = _adminManageSchool.GetSchool(nonExistingId);

                Assert.IsNull(school);
            }

            [TestMethod]
            public void GetSchool_WithNonExistingName_ReturnsEmptyCollection()
            {
                var nonExistingName = System.IO.Path.GetRandomFileName();

                var school = _adminManageSchool.GetSchool(nonExistingName);

                Assert.AreEqual<int>(0, school.Count());
            }

            [TestMethod]
            public void GetEmptySchool_ReturnsEmptySchoolObject()
            {
                var school = _adminManageSchool.GetEmptySchool();

                Assert.IsTrue(school is School);
                Assert.AreEqual<int>(default(int), school.SchoolId);
                Assert.AreEqual(null, school.SchoolName);
            }

            [TestMethod]
            public void UpdateSchool_WithValidNewData_UpdatesSchool()
            {
                var newSchoolName = "TestSchool_Updated";

                // Arrange - Get school to update
                var school = _db.Schools.Where(s => s.SchoolId == SchoolId).Single();
                school.SchoolName = newSchoolName;

                // Act - change name to "TestSchool_Updated"
                var updateResult = _adminManageSchool.UpdateSchool(school);

                // Assert
                // Updated school
                var updatedSchool = _db.Schools.Where(s => s.SchoolId == SchoolId).Single();

                Assert.AreEqual<bool>(true, updateResult);
                Assert.AreEqual(newSchoolName, updatedSchool.SchoolName);

                // Test clean up
                TestHelpers.RemoveSchool(newSchoolName);
            }

            [TestMethod]
            public void UpdateSchool_SchoolIdUpdateFails_ReturnsFalse()
            {
                /*
                 * Zmiana SchoolId nie jest możliwa przy naszej konfiguracji EF.
                 * Każda próba zmiany skutkuje Exception. 
                 * Nasze API zwraca infromację w postaci bool'a o tym czy update się powiódł czy nie, żeby nie męczyc się z wyjątkami.
                 */

                var schoolId = 123456798;

                // Arrange - Get school to update
                var school = _db.Schools.Where(s => s.SchoolId == SchoolId).Single();
                school.SchoolId = schoolId;

                // Act - change name to "TestSchool_Updated"
                var result = _adminManageSchool.UpdateSchool(school);

                Assert.AreEqual<bool>(false, result);
            }

        }
    }
}