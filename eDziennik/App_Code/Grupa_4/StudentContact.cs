﻿using eDziennik.App_SchoolDb;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data.Entity;
using System.Linq;

namespace eDziennik.App_Code.Grupa_4
{
    public class StudentContact : IStudentContact
    {
        private readonly SchoolDb _db;

        public StudentContact()
        {
            _db = new SchoolDb();
        }

        public Teacher GetTeacherById(int id)
        {
            return _db.Teachers.Where(t => t.TeacherId == id).SingleOrDefault();
        }


        public Teacher[] GetAllTeachers()
        {
            return _db.Teachers.ToArray();
        }


        public Student GetStudentById(int id)
        {
            return _db.Students.Where(s => s.StudentId == id).SingleOrDefault();
        }
    }

    public static class DatabaseTestHelpersStudentContact
    {
        public static void AddStudentToDB(Student student, Class studentClass)
        {
            using (var db = new SchoolDb())
            {
                db.Classes.Add(studentClass);
                db.Students.Add(student);
                db.SaveChanges();
            }
        }

        public static void AddTeacherToDB(Teacher teacher)
        {
            using (var db = new SchoolDb())
            {
                db.Teachers.Add(teacher);
                db.SaveChanges();
            }
        }

        public static void RemoveStudent(string studentLastName, int idClass)
        {
            using (var db = new SchoolDb())
            {
                foreach (var studentClass in db.Classes.Where(s => s.ClassId == idClass))
                {
                    db.Classes.Remove(studentClass);
                    db.Entry(studentClass).State = EntityState.Deleted;
                }

                foreach (var student in db.Students.Where(s => s.StudentLastName == studentLastName))
                {
                    db.Students.Remove(student);
                    db.Entry(student).State = EntityState.Deleted;
                }

                db.SaveChanges();
            }
        }

        public static void RemoveTeacher(string teacherLastName)
        {
            using (var db = new SchoolDb())
            {
                foreach (var teacher in db.Teachers.Where(s => s.TeacherLastName == teacherLastName))
                {
                    db.Teachers.Remove(teacher);
                    db.Entry(teacher).State = EntityState.Deleted;
                }

                db.SaveChanges();
            }
        }

        public static int GetStudentId(string studentLastName)
        {
            using (var db = new SchoolDb())
            {
                var students = db.Students.Where(s => s.StudentLastName == studentLastName);

                // wiem, że ogólnie taka sytuacja może sie zdarzyć - 
                //      nigdzie w specyfikacji nie ma nic o walidacji nazwy szkoły == mogą być dwie o tej samej nazwie
                // ponieważ jest to helperek na potrzeby mojego testu to nazwa szkoły musi być unikalna
                if (students.Count() != 1)
                    throw new InvalidOperationException("There was none or more than one student with this name.");

                return students.First().StudentId;
            }
        }

        public static int GetTeacherId(string teacherLastName)
        {
            using (var db = new SchoolDb())
            {
                var teachers = db.Teachers.Where(s => s.TeacherLastName == teacherLastName);

                // wiem, że ogólnie taka sytuacja może sie zdarzyć - 
                //      nigdzie w specyfikacji nie ma nic o walidacji nazwy szkoły == mogą być dwie o tej samej nazwie
                // ponieważ jest to helperek na potrzeby mojego testu to nazwa szkoły musi być unikalna
                if (teachers.Count() != 1)
                    throw new InvalidOperationException("There was none or more than one teacher with this name.");

                return teachers.First().TeacherId;
            }
        }
    }

    [TestClass]
    public class StudentContactTest
    {
        [TestClass]
        public class IntegrationTests
        {
            private readonly IStudentContact _studentContact;
            private readonly SchoolDb _db;

            private int teacherId;
            private int studentId;
            private string TeacherLastName = "Kowalski";
            private string StudentLastName = "Wieczny";

            public IntegrationTests()
            {
                _studentContact = new StudentContact();
                _db = new SchoolDb();
            }

            [TestInitialize]
            public void BeforeEachTest()
            {
                var studentClass = new Class
                {
                    ClassId = 1
                };

                var teacher = new Teacher
                {
                    TeacherLastName = TeacherLastName
                };

                var student = new Student
                {
                    StudentLastName = StudentLastName,
                    ClassId = studentClass.ClassId
                };

                DatabaseTestHelpersStudentContact.RemoveStudent(StudentLastName, student.ClassId);
                DatabaseTestHelpersStudentContact.RemoveTeacher(TeacherLastName);
                DatabaseTestHelpersStudentContact.AddStudentToDB(student, studentClass);
                DatabaseTestHelpersStudentContact.AddTeacherToDB(teacher);

                teacherId = DatabaseTestHelpersStudentContact.GetTeacherId(TeacherLastName);
                studentId = DatabaseTestHelpersStudentContact.GetStudentId(StudentLastName);
            }


            [TestMethod]
            public void GetStudent_WithValidId_ReturnsDesiredStudent()
            {
                var student = _studentContact.GetStudentById(studentId);

                Assert.AreEqual(studentId, student.StudentId);
                Assert.AreEqual(StudentLastName, student.StudentLastName);
            }

            [TestMethod]
            public void GetTeacher_WithValidId_ReturnsDesiredTeacher()
            {
                var teacher = _studentContact.GetTeacherById(teacherId);

                Assert.AreEqual(teacherId, teacher.TeacherId);
                Assert.AreEqual(TeacherLastName, teacher.TeacherLastName);
            }

            [TestMethod]
            public void GetTeacher_WithNonExistingId_ReturnsNull()
            {
                var nonExistingId = Int32.MaxValue;

                var teacher = _studentContact.GetTeacherById(nonExistingId);

                Assert.IsNull(teacher);
            }

            [TestMethod]
            public void GetStudent_WithNonExistingId_ReturnsNull()
            {
                var nonExistingId = Int32.MaxValue;

                var student = _studentContact.GetStudentById(nonExistingId);

                Assert.IsNull(student);
            }

            [TestMethod]
            public void GetAllTeachers_ReturnsArray()
            {
                var teachers = _studentContact.GetAllTeachers();

                Assert.IsInstanceOfType(teachers, typeof(Array));
            }

            [TestMethod]
            public void GetAllTeachers_ReturnsDesiredTeachers()
            {
                var teachers = _studentContact.GetAllTeachers();

                Assert.AreEqual(teacherId, teachers[teachers.Count() - 1].TeacherId);
                Assert.AreEqual(TeacherLastName, teachers[teachers.Count() - 1].TeacherLastName);
            }
        }
    }
}