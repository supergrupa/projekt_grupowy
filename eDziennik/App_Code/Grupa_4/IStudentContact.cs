﻿using eDziennik.App_SchoolDb;

namespace eDziennik.App_Code.Grupa_4
{
    public interface IStudentContact
    {
        Teacher GetTeacherById(int id);
        Teacher[] GetAllTeachers();
        Student GetStudentById(int id);
    }
}
