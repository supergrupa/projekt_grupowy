﻿using eDziennik.App_SchoolDb;
using System.Linq;

namespace eDziennik.App_Code.Grupa_4
{
    public interface IAdminManageSchool
    {
        void AddSchool(School school);
        School GetSchool(int id);
        IQueryable<School> GetSchool(string schoolName);
        School GetEmptySchool();
        bool UpdateSchool(School school);
    }
}
